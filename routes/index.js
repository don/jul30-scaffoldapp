const express = require('express');
const router = express.Router();
const knexConfig = require('../knexfile');
const env = process.env.NODE_ENV || 'development';
const knex = require('knex')(knexConfig[env]);

/* GET home page. */
router.get('/', function(req, res, next) {
  const title = 'ScaffoldApp';
  knex('scaffolds').then((scaffolds) => {
    res.render('index', { title, scaffolds });
  })
  .catch(err => console.error("WTF", err));
});

module.exports = router;
