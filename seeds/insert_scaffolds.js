
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('scaffolds').del()
    .then(function () {
      // Inserts seed entries
      return knex('scaffolds').insert([
        {name: "Big one", size: 10, material: "Stuff"},
        {name: "Small one", size: 1, material: "Stuff"},
        {name: "Medium one", size: 5, material: "Things"},
        {name: "Other one", size: 7, material: "Stuff"},
        {name: "Broken one", size: 0, material: "Garbage"}
      ]);
    });
};
