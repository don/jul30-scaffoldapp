
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('workers').del()
    .then(function () {
      // Inserts seed entries
      return knex('workers').insert([
      {name: "Joe Doe", wage: 2500, role: "Labourer"},
      {name: "Jane Doe", wage: 2500, role: "Labourer"},
      {name: "Jebediah Doe", wage: 2600, role: "Labourer"},
      {name: "John Doe", wage: 2500, role: "Foreman"}
  ]);
    });
};
