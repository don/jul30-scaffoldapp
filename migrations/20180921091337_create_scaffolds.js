
exports.up = function(knex, Promise) {
  return knex.schema.createTable('scaffolds', (t) => {
    t.increments();
    t.string('name');
    t.integer('size');
    t.string('material');
    t.timestamps();
  });  
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('scaffolds'); 
};
