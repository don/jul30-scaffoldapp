
exports.up = function(knex, Promise) {
  return knex.schema.createTable('workers', (t) => {
    t.increments();
    t.string('name');
    t.integer('wage');
    t.string('role');
    t.timestamps();
  });  
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('workers'); 
};
